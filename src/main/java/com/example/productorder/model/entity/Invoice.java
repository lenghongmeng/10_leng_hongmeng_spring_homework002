package com.example.productorder.model.entity;

import lombok.*;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Invoice {

    private Integer id;
    private Customer customer;
    private List<Product> products;
}
