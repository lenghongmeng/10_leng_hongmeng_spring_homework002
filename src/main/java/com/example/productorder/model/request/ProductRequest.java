package com.example.productorder.model.request;

import jakarta.validation.constraints.*;
import lombok.*;
import org.hibernate.validator.constraints.Length;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductRequest {

    @NotNull (message = "Product's name is required")
    @NotBlank (message = "Product's name must not be blank")
    @Length(max = 50, message = "Product's name must be less than or equal 50 characters")
    private String name;

    @NotNull (message = "Product's price must not be null")
    @Positive(message = "Product's price must greater than 0")
    private Double price;
}
