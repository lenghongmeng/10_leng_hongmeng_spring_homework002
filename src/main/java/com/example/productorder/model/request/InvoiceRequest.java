package com.example.productorder.model.request;

import jakarta.validation.constraints.*;
import lombok.*;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceRequest {

    @NotNull (message = "Customer id is required")
    @Positive (message = "Customer id must be greater than 0")
    private int customerId;

    @NotNull (message = "Product id in list is required")
    @NotEmpty (message = "Product id must have at least one id")
    private List<Integer> productIds;
}
