package com.example.productorder.model.request;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.validator.constraints.Length;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerRequest {

    @NotNull (message = "Customer's name is required")
    @NotBlank (message = "Customer's name must not be blank")
    @Length(max = 50, message = "Customer's name must be less than or equal 50 characters")
    private String name;

    private String address;

    @NotNull (message = "Customer's phone is required")
    @NotBlank (message = "Customer's phone must not be blank")
    @Length(max = 10, min = 9,message = "Customer's phone must be between 9 and 10")
    private String phone;
}
