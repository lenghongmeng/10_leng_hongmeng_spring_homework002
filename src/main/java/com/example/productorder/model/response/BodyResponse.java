package com.example.productorder.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BodyResponse<T> {
    private String message;
    private HttpStatus status;
    private Timestamp timestamp;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
}
