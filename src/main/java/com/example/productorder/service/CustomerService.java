package com.example.productorder.service;

import com.example.productorder.model.entity.Customer;
import com.example.productorder.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {
    List<Customer> getCustomers();
    Customer getCustomerById(Integer id);
    Integer storeCustomer(CustomerRequest customerRequest);
    Integer updateCustomer(CustomerRequest customerRequest, Integer id);
    Boolean deleteCustomer(Integer id);
}
