package com.example.productorder.service;

import com.example.productorder.model.entity.Product;
import com.example.productorder.model.request.ProductRequest;

import java.util.List;

public interface ProductService {
    List<Product> getProducts();
    Product getProductById(Integer id);
    Integer storeProduct(ProductRequest customerRequest);
    Integer updateProduct(ProductRequest customerRequest, Integer id);
    Boolean deleteProduct(Integer id);
}
