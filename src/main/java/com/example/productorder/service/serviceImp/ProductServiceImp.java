package com.example.productorder.service.serviceImp;

import com.example.productorder.model.entity.Product;
import com.example.productorder.model.request.ProductRequest;
import com.example.productorder.repository.ProductRepository;
import com.example.productorder.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {

    public final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getProducts() {
        return productRepository.findProducts();
    }

    @Override
    public Product getProductById(Integer id) {
        return productRepository.findProductById(id);
    }

    @Override
    public Integer storeProduct(ProductRequest customerRequest) {
        Integer customerId = productRepository.storeProduct(customerRequest);
        return customerId;
    }

    @Override
    public Integer updateProduct(ProductRequest customerRequest, Integer id) {
        return productRepository.updateProduct(customerRequest, id);
    }

    @Override
    public Boolean deleteProduct(Integer id) {
        return productRepository.deleteProduct(id);
    }
}
