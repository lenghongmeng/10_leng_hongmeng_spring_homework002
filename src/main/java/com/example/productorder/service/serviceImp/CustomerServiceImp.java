package com.example.productorder.service.serviceImp;

import com.example.productorder.model.entity.Customer;
import com.example.productorder.model.request.CustomerRequest;
import com.example.productorder.repository.CustomerRepository;
import com.example.productorder.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {

    public final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getCustomers() {
        return customerRepository.findCustomers();
    }

    @Override
    public Customer getCustomerById(Integer id) {
        return customerRepository.findCustomerById(id);
    }

    @Override
    public Integer storeCustomer(CustomerRequest customerRequest) {
        Integer customerId = customerRepository.storeCustomer(customerRequest);
        return customerId;
    }

    @Override
    public Integer updateCustomer(CustomerRequest customerRequest, Integer id) {
        return customerRepository.updateCustomer(customerRequest, id);
    }

    @Override
    public Boolean deleteCustomer(Integer id) {
        return customerRepository.deleteCustomer(id);
    }
}
