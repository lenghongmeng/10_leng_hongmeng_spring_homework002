package com.example.productorder.service.serviceImp;

import com.example.productorder.model.entity.Invoice;
import com.example.productorder.model.entity.Product;
import com.example.productorder.model.request.InvoiceRequest;
import com.example.productorder.model.request.ProductRequest;
import com.example.productorder.repository.InvoiceRepository;
import com.example.productorder.repository.ProductRepository;
import com.example.productorder.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImp implements InvoiceService {

    public final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getInvoices() {
        return invoiceRepository.findInvoices();
    }

    @Override
    public Invoice getInvoiceById(Integer id) {
        return invoiceRepository.findInvoiceById(id);
    }

    @Override
    public Integer storeInvoice(InvoiceRequest invoiceRequest) {
        Integer invoiceId = invoiceRepository.storeInvoice(invoiceRequest);
        for (Integer productId : invoiceRequest.getProductIds()) {
            invoiceRepository.storeIntoInvoiceDetail(invoiceId, productId);
        }
        return invoiceId;
    }

    @Override
    public Integer updateInvoice(InvoiceRequest invoiceRequest, Integer id) {
        Integer invoiceId = invoiceRepository.updateInvoice(invoiceRequest, id);
        invoiceRepository.deleteIntoInvoiceDetail(invoiceId);
        for (Integer productId : invoiceRequest.getProductIds()) {
            invoiceRepository.updateIntoInvoiceDetail(invoiceId, productId);
        }
        return invoiceId;
    }

    @Override
    public Boolean deleteInvoice(Integer id) {
        return invoiceRepository.deleteInvoice(id);
    }
}
