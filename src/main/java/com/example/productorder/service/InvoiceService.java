package com.example.productorder.service;

import com.example.productorder.model.entity.Invoice;
import com.example.productorder.model.request.InvoiceRequest;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getInvoices();
    Invoice getInvoiceById(Integer id);
    Integer storeInvoice(InvoiceRequest invoiceRequest);
    Integer updateInvoice(InvoiceRequest invoiceRequest, Integer id);
    Boolean deleteInvoice(Integer id);
}
