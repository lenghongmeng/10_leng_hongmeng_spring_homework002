package com.example.productorder.controller;

import com.example.productorder.model.entity.Invoice;
import com.example.productorder.model.entity.Product;
import com.example.productorder.model.request.InvoiceRequest;
import com.example.productorder.model.response.BodyResponse;
import com.example.productorder.service.InvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/invoices")
public class InvoiceController {

    private String message;
    private HttpStatus httpResponse;
    private Invoice invoice;
    private List<Invoice> listInvoices;
    private Timestamp getTimestamp() {
       return new Timestamp(System.currentTimeMillis());
    }

    private <T> ResponseEntity getResponse(String message, HttpStatus httpStatus, T data) {
        BodyResponse<T> invoiceResponse = BodyResponse.<T>builder()
                .message(message)
                .status(httpStatus)
                .timestamp(getTimestamp())
                .payload(data)
                .build();
        return new ResponseEntity(invoiceResponse, httpStatus);
    }

    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/list")
    @Operation(summary = "Get all invoices from database")
    public ResponseEntity<BodyResponse<List<Invoice>>> getInvoices() {

        if (invoiceService.getInvoices().size() != 0) {
            message = "Get invoices successfully";
            httpResponse = HttpStatus.OK;
            listInvoices = invoiceService.getInvoices();

        } else {
            message = "Invoices has no data";
            httpResponse = HttpStatus.NOT_FOUND;
            listInvoices = null;
        }

        return getResponse(message, httpResponse, listInvoices);
    }

    @GetMapping("/show/{id}")
    @Operation(summary = "Get specific invoice by id")
    public ResponseEntity<BodyResponse> getInvoiceById(@PathVariable Integer id) {

        if(invoiceService.getInvoiceById(id) != null) {
            message = "Get invoice successfully";
            httpResponse = HttpStatus.OK;
            invoice = invoiceService.getInvoiceById(id);
        } else {
            message = "Cannot get invoice";
            httpResponse = HttpStatus.NOT_FOUND;
            invoice = null;
        }

        return getResponse(message, httpResponse, invoice);
    }

    @PostMapping("/store")
    @Operation(summary = "Add new invoice into database")
    public ResponseEntity<BodyResponse<Invoice>> storeInvoice(@Valid @RequestBody InvoiceRequest invoiceRequest) {

        Integer invoiceId = invoiceService.storeInvoice(invoiceRequest);

        if(invoiceId != null) {
            message = "Invoice was added successfully";
            httpResponse = HttpStatus.OK;
            invoice = invoiceService.getInvoiceById(invoiceId);
        } else {
            message = "Cannot add invoice";
            httpResponse = HttpStatus.BAD_REQUEST;
            invoice = null;
        }

        return getResponse(message, httpResponse, invoice);
    }

    @PutMapping("/update/{id}")
    @Operation(summary = "Update specific invoice by id")
    public ResponseEntity<BodyResponse<Invoice>> updateInvoice(@Valid @RequestBody InvoiceRequest invoiceRequest, @PathVariable Integer id) {

        Integer invoiceId = invoiceService.updateInvoice(invoiceRequest, id);

        if(invoiceService.getInvoiceById(id) != null) {
            message = "Invoice was updated successfully";
            httpResponse = HttpStatus.OK;
            invoice = invoiceService.getInvoiceById(invoiceId);
        } else {
            message = "Cannot update invoice";
            httpResponse = HttpStatus.NOT_FOUND;
            invoice = null;
        }

        return getResponse(message, httpResponse, invoice);
    }

    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Delete specific invoice by id")
    public ResponseEntity<Product> deleteInvoice(@PathVariable Integer id) {

        invoice = invoiceService.getInvoiceById(id);

        if(invoice != null) {
            invoiceService.deleteInvoice(id);
            message = "Invoice was deleted successfully";
            httpResponse = HttpStatus.OK;
        } else {
            message = "Cannot delete product";
            httpResponse = HttpStatus.NOT_FOUND;
        }

        return getResponse(message, httpResponse, null);
    }
}