package com.example.productorder.controller;

import com.example.productorder.model.entity.Product;
import com.example.productorder.model.request.ProductRequest;
import com.example.productorder.model.response.BodyResponse;
import com.example.productorder.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    private String message;
    private HttpStatus httpResponse;
    private Product product;
    private List<Product> listProducts;
    private Timestamp getTimestamp() {
       return new Timestamp(System.currentTimeMillis());
    }

    private <T> ResponseEntity getResponse(String message, HttpStatus httpStatus, T data) {
        BodyResponse<T> productResponse = BodyResponse.<T>builder()
                .message(message)
                .status(httpStatus)
                .timestamp(getTimestamp())
                .payload(data)
                .build();
        return new ResponseEntity(productResponse, httpStatus);
    }

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }
    @GetMapping("/list")
    @Operation(summary = "Get all product from database")
    public ResponseEntity<BodyResponse<List<Product>>> getProducts() {

        if (productService.getProducts().size() != 0) {
            message = "Get products successfully";
            httpResponse = HttpStatus.OK;
            listProducts = productService.getProducts();

        } else {
            message = "Product has no data";
            httpResponse = HttpStatus.NOT_FOUND;
            listProducts = null;
        }

        return getResponse(message, httpResponse, listProducts);
    }

    @GetMapping("/show/{id}")
    @Operation(summary = "Get specific product by id")
    public ResponseEntity<BodyResponse> getProductById(@PathVariable Integer id) {

        if(productService.getProductById(id) != null) {
            message = "Get product successfully";
            httpResponse = HttpStatus.OK;
            product = productService.getProductById(id);
        } else {
            message = "Cannot get product";
            httpResponse = HttpStatus.NOT_FOUND;
            product = null;
        }

        return getResponse(message, httpResponse, product);
    }

    @PostMapping("/store")
    @Operation(summary = "Add new product into database")
    public ResponseEntity<BodyResponse<Product>> storeProduct(@Valid @RequestBody ProductRequest productRequest) {

        Integer productId = productService.storeProduct(productRequest);

        if(productId != null) {
            message = "Product was added successfully";
            httpResponse = HttpStatus.OK;
            product = productService.getProductById(productId);
        } else  {
            message = "Cannot add product";
            httpResponse = HttpStatus.BAD_REQUEST;
            product = null;
        }

        return getResponse(message, httpResponse, product);
    }

    @PutMapping("/update/{id}")
    @Operation(summary = "Update specific product by id")
    public ResponseEntity<BodyResponse<Product>> updateProduct(@Valid @RequestBody ProductRequest productRequest, @PathVariable Integer id) {

        Integer productId = productService.updateProduct(productRequest, id);

        if(productService.getProductById(id) != null) {
            message = "Product was updated successfully";
            httpResponse = HttpStatus.OK;
            product = productService.getProductById(productId);
        } else {
            message = "Cannot update product";
            httpResponse = HttpStatus.NOT_FOUND;
            product = null;
        }

        return getResponse(message, httpResponse, product);
    }

    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Delete specific product by id")
    public ResponseEntity<Product> deleteProduct(@PathVariable Integer id) {

        product = productService.getProductById(id);

        if(product != null) {
            productService.deleteProduct(id);
            message = "Product was deleted successfully";
            httpResponse = HttpStatus.OK;
        } else {
            message = "Cannot delete product";
            httpResponse = HttpStatus.NOT_FOUND;
        }

        return getResponse(message, httpResponse, null);
    }
}