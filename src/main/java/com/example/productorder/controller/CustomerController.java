package com.example.productorder.controller;

import com.example.productorder.model.entity.Customer;
import com.example.productorder.model.request.CustomerRequest;
import com.example.productorder.model.response.BodyResponse;
import com.example.productorder.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {

    private String message;
    private HttpStatus httpResponse;
    private Customer customer;
    private List<Customer> listCustomers;
    private Timestamp getTimestamp() {
       return new Timestamp(System.currentTimeMillis());
    }

    private <T> ResponseEntity getResponse(String message, HttpStatus httpStatus, T data) {
        BodyResponse<T> customerResponse = BodyResponse.<T>builder()
                .message(message)
                .status(httpStatus)
                .timestamp(getTimestamp())
                .payload(data)
                .build();
        return new ResponseEntity(customerResponse, httpStatus);
    }

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }
    @GetMapping("/list")
    @Operation(summary = "Get all customer from database")
    public ResponseEntity<BodyResponse<List<Customer>>> getCustomers() {

        if (customerService.getCustomers().size() != 0) {
            message = "Get customers successfully";
            httpResponse = HttpStatus.OK;
            listCustomers = customerService.getCustomers();

        } else {
            message = "Customer has no data";
            httpResponse = HttpStatus.NOT_FOUND;
            listCustomers = null;
        }

        return getResponse(message, httpResponse, listCustomers);
    }

    @GetMapping("/show/{id}")
    @Operation(summary = "Get specific customer by id")
    public ResponseEntity<BodyResponse> getCustomerById(@PathVariable Integer id) {

        if(customerService.getCustomerById(id) != null) {
            message = "Get customer successfully";
            httpResponse = HttpStatus.OK;
            customer = customerService.getCustomerById(id);
        } else {
            message = "Cannot get customer";
            httpResponse = HttpStatus.NOT_FOUND;
            customer = null;
        }

        return getResponse(message, httpResponse, customer);
    }

    @PostMapping("/store")
    @Operation(summary = "Add new customer into database")
    public ResponseEntity<BodyResponse<Customer>> storeCustomer(@Valid @RequestBody CustomerRequest customerRequest) {

        Integer customerId = customerService.storeCustomer(customerRequest);

        if(customerId != null) {
            message = "Customer was added successfully";
            httpResponse = HttpStatus.OK;
            customer = customerService.getCustomerById(customerId);
        } else  {
            message = "Cannot add customer";
            httpResponse = HttpStatus.BAD_REQUEST;
            customer = null;
        }

        return getResponse(message, httpResponse, customer);
    }

    @PutMapping("/update/{id}")
    @Operation(summary = "Update specific customer by id")
    public ResponseEntity<BodyResponse<Customer>> updateCustomer(@Valid @RequestBody CustomerRequest customerRequest, @PathVariable Integer id) {

        Integer customerId = customerService.updateCustomer(customerRequest, id);

        if(customerService.getCustomerById(id) != null) {
            message = "Customer was updated successfully";
            httpResponse = HttpStatus.OK;
            customer = customerService.getCustomerById(customerId);
        } else {
            message = "Cannot update customer";
            httpResponse = HttpStatus.NOT_FOUND;
            customer = null;
        }

        return getResponse(message, httpResponse, customer);
    }

    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Delete specific customer by id")
    public ResponseEntity<Customer> deleteCustomer(@PathVariable Integer id) {

        Customer customer = customerService.getCustomerById(id);

        if(customer != null) {
            customerService.deleteCustomer(id);
            message = "Customer was deleted successfully";
            httpResponse = HttpStatus.OK;
        } else {
            message = "Cannot delete customer";
            httpResponse = HttpStatus.NOT_FOUND;
        }

        return getResponse(message, httpResponse, null);
    }
}