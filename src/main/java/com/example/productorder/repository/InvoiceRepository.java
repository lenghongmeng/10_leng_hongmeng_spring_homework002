package com.example.productorder.repository;

import com.example.productorder.model.entity.Invoice;
import com.example.productorder.model.entity.Product;
import com.example.productorder.model.request.InvoiceRequest;
import com.example.productorder.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {

    // Select all invoices
    @Select("SELECT * FROM invoices ORDER BY id ASC")
    @Results(
            id = "invoiceMapper",
            value = {
                    @Result(property = "id", column = "id"),
                    @Result(property = "customer", column = "customer_id",
                            one = @One(select = "com.example.productorder.repository.CustomerRepository.findCustomerById")
                    ),
                    @Result(property = "products", column = "id",
                            many = @Many(select = "com.example.productorder.repository.ProductRepository.getProductsByInvoiceId")
                    )
            }
    )
    List<Invoice> findInvoices();

    // Select specific invoice by id
    @Select("SELECT * FROM invoices WHERE id = #{id}")
    @ResultMap("invoiceMapper")
    Invoice findInvoiceById(Integer id);

    // Insert new invoice to database
    @Select("INSERT INTO invoices(customer_id) VALUES(#{request.customerId}) RETURNING id")
    Integer storeInvoice(@Param("request") InvoiceRequest invoiceRequest);

    @Insert("INSERT INTO invoice_detail(invoice_id, product_id) VALUES(#{invoiceId}, #{productId})")
    void storeIntoInvoiceDetail(Integer invoiceId, Integer productId);

    // Delete invoice
    @Delete("DELETE FROM invoices WHERE id = #{id}")
    Boolean deleteInvoice(Integer id);

    // Update invoice
    @Select("UPDATE invoices SET customer_id = #{request.customerId} WHERE id = #{id} RETURNING id")
    Integer updateInvoice(@Param("request") InvoiceRequest invoiceRequest, Integer id);
    @Delete("DELETE FROM invoice_detail WHERE invoice_id = #{invoiceId}")
    void deleteIntoInvoiceDetail(Integer invoiceId);
    //    @Update("UPDATE invoice_detail SET invoice_id = #{invoiceId}, product_id = #{productId}")
    @Insert("INSERT INTO invoice_detail(invoice_id, product_id) VALUES(#{invoiceId}, #{productId})")
    void updateIntoInvoiceDetail(Integer invoiceId, Integer productId);

}
