package com.example.productorder.repository;

import com.example.productorder.model.entity.Product;
import com.example.productorder.model.request.ProductRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProductRepository {

    @Select("SELECT * FROM products ORDER BY id ASC")
    List<Product> findProducts();

    @Select("SELECT * FROM products WHERE id = #{id}")
    Product findProductById(Integer id);

    @Select("INSERT INTO products(name, price) VALUES(#{request.name}, #{request.price}) RETURNING id")
    Integer storeProduct(@Param("request") ProductRequest productRequest);

    @Select("UPDATE products SET name = #{request.name}, price = #{request.price} " +
            "WHERE id = #{id} RETURNING id")
    Integer updateProduct(@Param("request") ProductRequest productRequest, Integer id);

    @Delete("DELETE FROM products WHERE id = #{id}")
    Boolean deleteProduct(Integer id);

    @Select("SELECT p.id, p.name, p.price " +
            "FROM products p INNER JOIN invoice_detail inv " +
            "ON p.id = inv.product_id " +
            "WHERE invoice_id = #{invoiceId}")
    List<Product> getProductsByInvoiceId(Integer invoiceId);
}
