package com.example.productorder.repository;

import com.example.productorder.model.entity.Customer;
import com.example.productorder.model.request.CustomerRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CustomerRepository {

    @Select("SELECT * FROM customers ORDER BY id ASC")
    List<Customer> findCustomers();

    @Select("SELECT * FROM customers WHERE id = #{id}")
    Customer findCustomerById(Integer id);

    @Select("INSERT INTO customers(name, address, phone) VALUES(#{request.name}, #{request.address}, #{request.phone}) RETURNING id")
    Integer storeCustomer(@Param("request") CustomerRequest customerRequest);

    @Select("UPDATE customers SET name = #{request.name}, address = #{request.address}, phone = #{request.phone} " +
            "WHERE id = #{id} RETURNING id")
    Integer updateCustomer(@Param("request") CustomerRequest customerRequest, Integer id);

    @Delete("DELETE FROM customers WHERE id = #{id}")
    Boolean deleteCustomer(Integer id);
}
