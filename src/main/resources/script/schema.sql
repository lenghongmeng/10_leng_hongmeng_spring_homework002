create database product_order;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
SELECT uuid_generate_v4();


CREATE TABLE customers(
--     id uuid DEFAULT uuid_generate_v4() primary key,
    id SERIAL PRIMARY KEY ,
    name VARCHAR(50) NOT NULL ,
    address VARCHAR(250),
    phone VARCHAR(50) NOT NULL
);

CREATE TABLE products(
     id SERIAL PRIMARY KEY ,
     name VARCHAR(50) NOT NULL ,
     price DECIMAL(8, 2) NOT NULL CHECK ( price > 0 )
);

CREATE TABLE invoices(
    id SERIAL PRIMARY KEY ,
    date DATE DEFAULT CURRENT_DATE,
    customer_id INT NOT NULL REFERENCES customers(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE invoice_detail(
    id SERIAL PRIMARY KEY ,
    invoice_id INT NOT NULL REFERENCES invoices(id) ON DELETE CASCADE ON UPDATE CASCADE ,
    product_id INT NOT NULL REFERENCES products(id) ON DELETE CASCADE ON UPDATE CASCADE
);

drop table products;
drop table customers;
drop table invoices;
drop table invoice_detail;